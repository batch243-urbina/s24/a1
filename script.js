console.log(`Hello Batch243`);

const getCube = function (number) {
  let answer = Math.pow(number, 3);
  console.log(`The cube of ${number} is ${answer}`);
};

const address = [258, "Washington Ave. NW", "California", "90011"];
const [houseNumber, houseStreet, houseState, houseZipCode] = address;
console.log(
  `I live at ${houseNumber} ${houseStreet}, ${houseState} ${houseZipCode}`
);

const animal = {
  name: "Lolong",
  species: "Saltwater Crocodile",
  weight: "1075",
  measurement: "20 ft 3 in",
};

const { name, species, weight, measurement } = animal;
console.log(
  `${name} was a ${species.toLowerCase()}. He weighed at ${weight} kgs with a measurement of ${measurement}.`
);

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(`${number}`));

const reduceNumber = numbers.reduce((x, y) => x + y);
console.log(reduceNumber);

class Dog {
  constructor(name, age, breed) {
    this.dogName = name;
    this.dogAge = age;
    this.dogBreed = breed;
  }
}

const dog1 = new Dog("Happy", 28, "Beagle");
console.log(dog1);
